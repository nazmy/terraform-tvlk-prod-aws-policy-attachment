module "iam" {
  source       = "github.com/traveloka/terraform-aws-iam-role.git//modules/instance?ref=v0.6.0"
  service_name = "${local.service_name}"
  cluster_role = "${local.service_role}"
}

module "attach" {
  source         = "../modules/app/"
  role_name      = "${module.iam.role_name}"
  product_domain = "${local.product_domain}"
  service_name   = "tsitest"
}
