variable "role_name" {
  description = "The name of the role. It will forces new resource on change."
  type        = "string"
}

variable "product_domain" {
  description = "The product_domain identifier"
  type        = "string"
}

variable "service_name" {
  description = "The service_name identifier"
  type        = "string"
}
