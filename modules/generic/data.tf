data "aws_region" "current" {}

data "aws_kms_alias" "tvlk_secret" {
  name = "alias/tvlk/ssm/tvlk-secret"
}

data "aws_iam_policy_document" "parameter_store_readonly" {
  statement {
    sid    = "AllowDescribeParams"
    effect = "Allow"

    actions = [
      "ssm:DescribeParameters",
    ]

    resources = ["*"]
  }

  statement {
    sid    = "AllowReadUserParams"
    effect = "Allow"

    actions = [
      "ssm:GetParametersByPath",
      "ssm:GetParameters",
      "ssm:GetParameterHistory",
      "ssm:GetParameter",
    ]

    resources = [
      "arn:aws:ssm:*:*:parameter/tvlk-secret/${var.service_name}/*",
    ]
  }

  statement {
    sid    = "AllowDecryptDescribe"
    effect = "Allow"

    actions = [
      "kms:DescribeKey",
      "kms:Decrypt",
    ]

    resources = [
      "arn:aws:kms:${data.aws_region.current.name}:${module.this.aws_account_id}:key/${data.aws_kms_alias.tvlk_secret.target_key_id}",
    ]
  }
}

