module "this" {
  source         = "../../"
  role_name      = "${var.role_name}"
  product_domain = "${var.product_domain}"
}

resource "aws_iam_role_policy" "parameter_store_readonly" {
  name   = "parameter_store_readonly"
  role   = "${var.role_name}"
  policy = "${data.aws_iam_policy_document.parameter_store_readonly.json}"
}

