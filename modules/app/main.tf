module "this" {
  source         = "../../"
  role_name      = "${var.role_name}"
  product_domain = "${var.product_domain}"
}

resource "aws_iam_role_policy_attachment" "policy_attachment_commonbeServer" {
  role       = "${var.role_name}"
  policy_arn = "arn:aws:iam::${module.this.aws_account_id}:policy/CommonBEServer"
}

resource "aws_iam_role_policy" "parameter_store_readonly" {
  name   = "parameter_store_readonly"
  role   = "${var.role_name}"
  policy = "${data.aws_iam_policy_document.parameter_store_readonly.json}"
}

