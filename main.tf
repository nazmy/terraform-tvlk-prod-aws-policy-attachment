data "aws_caller_identity" "aws_account" {}

resource "aws_iam_role_policy_attachment" "policy_attachment_commonec2" {
  role       = "${var.role_name}"
  policy_arn = "arn:aws:iam::${data.aws_caller_identity.aws_account.account_id}:policy/CommonEC2"
}

resource "aws_iam_role_policy_attachment" "policy_attachment_commonec2pd" {
  role       = "${var.role_name}"
  policy_arn = "arn:aws:iam::${data.aws_caller_identity.aws_account.account_id}:policy/CommonEC2${var.product_domain}"
}
